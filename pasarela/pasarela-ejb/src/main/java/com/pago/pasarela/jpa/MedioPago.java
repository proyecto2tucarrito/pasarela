
package com.pago.pasarela.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
@NamedQuery(name="MedioPago.existe", query="SELECT t FROM MedioPago t WHERE t.id = :id"),
@NamedQuery(name="MedioPago.findAll", query="SELECT t FROM MedioPago t")
})
public class MedioPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="idMedioPago", nullable=true)
	private List<Plan> plan;
	
	public MedioPago() {}
	
	public MedioPago(String id,  String nombre,List<Plan> plan) {
		super();
		this.id = id;
		this.plan = plan;

	}

	public List<Plan> getPlan() {
		return plan;
	}

	public void setPlan(List<Plan> plan) {
		this.plan = plan;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
