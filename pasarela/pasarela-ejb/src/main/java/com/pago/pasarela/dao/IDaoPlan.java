package com.pago.pasarela.dao;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.Plan;

public interface IDaoPlan {
	public void insert(EntityManager em, Plan plan) throws Exception;
	public Plan getPlan(EntityManager em, int id) throws Exception;
	public void update(EntityManager em, Plan plan) throws Exception;
	public void delete(EntityManager em, int id) throws Exception;
	
	
}
