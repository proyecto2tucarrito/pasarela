package com.pago.pasarela.ejb;


import javax.ejb.Local;

import com.pago.pasarela.jpa.Plan;


@Local
public interface IEjbPlan {
	public void insert(Plan plan) throws Exception;
	public Plan getPlan(int plan) throws Exception;
	public void update(Plan plan) throws Exception;
	public void delete(int plan) throws Exception;
}
