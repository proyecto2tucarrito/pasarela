package com.pago.pasarela.ejb;

import java.util.List;

import javax.ejb.Local;

import com.pago.pasarela.jpa.MedioPago;


@Local
public interface IEjbMedioPago {
	public void insert(MedioPago medioPago) throws Exception;
	public MedioPago getMedioPago(String id) throws Exception;
	public void update(MedioPago medioPago) throws Exception;
	public void delete(String id) throws Exception;
	public List<MedioPago> getAllMedioPago() throws Exception;
}
