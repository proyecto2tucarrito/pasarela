package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.Tarjeta;

public interface IDaoTarjeta {
	public void insert(EntityManager em, Tarjeta tarjeta) throws Exception;
	public Tarjeta getTarjetaById(EntityManager em, String idTarjeta) throws Exception;
	public void update(EntityManager em, Tarjeta tarjeta) throws Exception;

}


