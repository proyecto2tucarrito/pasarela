package com.pago.pasarela.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@NamedQueries({
@NamedQuery(name="Usuario.login", query="SELECT u FROM Usuario u WHERE u.nombre = :email and u.pass = :pass"),
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u WHERE u.admin=false")
})
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Length(max=255)
	private String nombre;

	@NotNull
	private byte[] pass;
	
	@NotNull
	private boolean admin;
	
	@NotNull
	private byte[] token;
	
	@NotNull
	private boolean activo;
	
	public Usuario() {}
	
	public Usuario(String nombre, byte[] pass, boolean admin, byte[] token, boolean activo) {
		super();
		this.nombre = nombre;
		this.pass = pass;
		this.admin = admin;
		this.token = token;
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public byte[] getPass() {
		return pass;
	}

	public void setPass(byte[] pass) {
		this.pass = pass;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public byte[] getToken() {
		return token;
	}

	public void setToken(byte[] token) {
		this.token = token;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	
	

	
}
