package com.pago.pasarela.ejb;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.pago.pasarela.dao.DaoPlan;
import com.pago.pasarela.dao.IDaoPlan;
import com.pago.pasarela.jpa.Plan;

@Stateless
@LocalBean
public class EjbPlan implements IEjbPlan {
	
	@PersistenceContext
	private EntityManager em = null;
	
	public EjbPlan()
	{
	}
	
	@Override
	public void insert(Plan plan) throws Exception {
		IDaoPlan iDaoPlan = new DaoPlan();
		try {
			iDaoPlan.insert(em, plan);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Plan getPlan(int id) throws Exception {
		IDaoPlan iDaoPlan = new DaoPlan();
		try {
			return iDaoPlan.getPlan(em, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void update(Plan plan) throws Exception {
		IDaoPlan iDaoPlan = new DaoPlan();
		try {
			iDaoPlan.update(em, plan);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void delete(int id) throws Exception {
		IDaoPlan iDaoPlan = new DaoPlan();
		try {
			iDaoPlan.delete(em, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

}

