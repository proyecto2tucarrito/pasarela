package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.TarjetaCliente;

public interface IDaoTarjetaCliente {
	public void insert(EntityManager em, TarjetaCliente tarjeta) throws Exception;
	public TarjetaCliente getTarjetaById(EntityManager em, byte[] numeroTarjeta) throws Exception;
	public void update(EntityManager em, TarjetaCliente tarjeta) throws Exception;

}


