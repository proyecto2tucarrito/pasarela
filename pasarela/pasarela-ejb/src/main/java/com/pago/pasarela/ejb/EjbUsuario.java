package com.pago.pasarela.ejb;


import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.pago.pasarela.dao.DaoUsuario;
import com.pago.pasarela.dao.IDaoUsuario;
import com.pago.pasarela.jpa.Usuario;

@Stateless
@LocalBean
public class EjbUsuario implements IEjbUsuario {
	
	@PersistenceContext
	private EntityManager em = null;
	
	public EjbUsuario()
	{
	}
	
	@Override
	public void insert(Usuario usuario) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		try {
			iDaoUsuario.insert(em, usuario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Usuario getUsuarioById(String idUsr) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		try {
			return iDaoUsuario.getUsuarioById(em, idUsr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void update(Usuario usuario) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		try {
			iDaoUsuario.update(em, usuario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void delete(String idUsr) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		try {
			iDaoUsuario.delete(em, idUsr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void bloquear(String idUsr) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		try {
			Usuario usuario = iDaoUsuario.getUsuarioById(em, idUsr);
			usuario.setActivo(!(usuario.isActivo()));
			iDaoUsuario.update(em, usuario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Usuario> getAllUsuario() throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		try {
			return iDaoUsuario.getAllUsuario(em);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Usuario iniciarSesion(String idUsr,byte[] pass) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		Usuario usuario = null;
		
		try {
			usuario = iDaoUsuario.getUsuarioLogin(em, idUsr, pass);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		
		return usuario;
	}
	

}

