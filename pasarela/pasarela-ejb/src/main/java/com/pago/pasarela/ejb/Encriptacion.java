package com.pago.pasarela.ejb;


import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class Encriptacion {
    int aesKeySize = 256;
    
    private SecretKeySpec generateAesKey(String clave2) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    String key1 = clave2;//"770A8A65DA156D24EE2A093277530142";
    byte[] aesKey1 = key1.getBytes("UTF-8");    
    return aesKeySpec(aesKey1);
  }    
    
  private SecretKeySpec aesKeySpec(byte[] key ){
    return new SecretKeySpec(key, "AES")   ;
  }

  
 private byte[] aesCipher(int mode,SecretKeySpec keySpec,byte[] ivBytes,String plainText) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
    cipher.init(mode, keySpec, ivSpec);
    byte[] encryptedTextBytes = cipher.doFinal(plainText.getBytes("UTF-8"));
    return encryptedTextBytes;
  }
    
  public byte[] encryptAes(String clave1, String clave2,String texto  ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
    
      SecretKeySpec sks=generateAesKey(clave2);
      byte[] b=aesCipher(Cipher.ENCRYPT_MODE, sks, Base64.getDecoder().decode(clave1.getBytes("utf-8")),texto);
      return b;
  }      
  
}

