package com.pago.pasarela.ejb;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.Compra;
import com.pago.pasarela.jpa.CompraWS;
@Local
public interface IEjbCompra {
	
	public void insert(CompraWS compra) throws Exception;
	public Compra getCompraById(int idCompra) throws Exception;
	public void update(Compra compra) throws Exception;
	public void delete(int idCompra) throws Exception;
	public List<Compra> getAllCompra() throws Exception;
	public Compra encriptarCompra(CompraWS compraWs) throws Exception ;

}
