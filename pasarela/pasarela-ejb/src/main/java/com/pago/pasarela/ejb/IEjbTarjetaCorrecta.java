package com.pago.pasarela.ejb;
import javax.ejb.Local;

import com.pago.pasarela.jpa.Tarjeta;
import com.pago.pasarela.jpa.TarjetaCorrecta;

@Local
public interface IEjbTarjetaCorrecta {
	
	public void insert(Tarjeta tarjeta) throws Exception;
	public TarjetaCorrecta getTarjetaById(byte[] numeroTarjeta) throws Exception;
	public void update(Tarjeta tarjeta) throws Exception;
	public boolean existeTarjeta(byte[] numeroTarjeta) throws Exception;
	public TarjetaCorrecta encriptarTarjeta(Tarjeta tarjetaWS) throws Exception;
	
}