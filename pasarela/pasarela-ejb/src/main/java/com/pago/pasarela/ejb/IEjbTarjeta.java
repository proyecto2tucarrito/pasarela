package com.pago.pasarela.ejb;

import java.util.List;

import javax.ejb.Local;

import com.pago.pasarela.jpa.Tarjeta;

@Local
public interface IEjbTarjeta {
	
	public void insert(Tarjeta tarjeta) throws Exception;
	public Tarjeta getTarjetaById(String idTarjeta) throws Exception;
	public void update(Tarjeta tarjeta) throws Exception;
	public boolean existeTarjeta(String idTarjeta) throws Exception;
	

}
