package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import com.pago.pasarela.jpa.TarjetaCorrecta;

public class DaoTarjetaCorrecta implements IDaoTarjetaCorrecta{

	public DaoTarjetaCorrecta() {
	
	}
	@Override
	public void insert(EntityManager em, TarjetaCorrecta tarjeta) {
		em.persist(tarjeta);
	}
	
	@Override
	public TarjetaCorrecta getTarjetaById(EntityManager em, byte[] numeroTarjeta) {
		try {
		return em.createNamedQuery("TarjetaCorrecta.existe",TarjetaCorrecta.class).setParameter("numero", numeroTarjeta).getSingleResult();
		}
		catch (NoResultException nre){
			return null;
		}
	}
	
	@Override
	public void update(EntityManager em, TarjetaCorrecta tarjeta) {
		em.merge(tarjeta);
	}
	
}