package com.pago.pasarela.dao;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.TarjetaCorrecta;

public interface IDaoTarjetaCorrecta {
	public void insert(EntityManager em, TarjetaCorrecta tarjeta) throws Exception;
	public TarjetaCorrecta getTarjetaById(EntityManager em, byte[] numeroTarjeta) throws Exception;
	public void update(EntityManager em, TarjetaCorrecta tarjeta) throws Exception;

}


