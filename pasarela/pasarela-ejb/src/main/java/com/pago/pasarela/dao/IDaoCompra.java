package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.Compra;

public interface IDaoCompra {
	public void insert(EntityManager em, Compra compra) throws Exception;
	public Compra getCompraById(EntityManager em, int idCompra) throws Exception;
	public void update(EntityManager em, Compra compra) throws Exception;
	public void delete(EntityManager em, int idCompra) throws Exception;
	public List<Compra> getAllCompra(EntityManager em) throws Exception;
	
}
