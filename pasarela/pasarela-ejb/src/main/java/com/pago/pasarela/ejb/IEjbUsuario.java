package com.pago.pasarela.ejb;

import java.util.List;

import javax.ejb.Local;

import com.pago.pasarela.jpa.Usuario;


@Local
public interface IEjbUsuario {
	public void insert(Usuario usuario) throws Exception;
	public Usuario getUsuarioById(String idUsr) throws Exception;
	public void update(Usuario usuario) throws Exception;
	public void delete(String idUsr) throws Exception;
	public void bloquear(String idUsr) throws Exception;
	public List<Usuario> getAllUsuario() throws Exception;
	public Usuario iniciarSesion(String idUsr,byte[] pass) throws Exception;
}
