package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import com.pago.pasarela.jpa.TarjetaCliente;
import com.pago.pasarela.jpa.Usuario;

public class DaoTarjetaCliente implements IDaoTarjetaCliente{

	public DaoTarjetaCliente() {
	
	}
	@Override
	public void insert(EntityManager em, TarjetaCliente tarjeta) {
		em.persist(tarjeta);
	}
	
	@Override
	public TarjetaCliente getTarjetaById(EntityManager em, byte[] numeroTarjeta) {
		try {
		return em.createNamedQuery("TarjetaCliente.existe",TarjetaCliente.class).setParameter("numero", numeroTarjeta).getSingleResult();
		}
		catch (NoResultException nre){
			return null;
		}
	}
	
	@Override
	public void update(EntityManager em, TarjetaCliente tarjeta) {
		em.merge(tarjeta);
	}
	
}