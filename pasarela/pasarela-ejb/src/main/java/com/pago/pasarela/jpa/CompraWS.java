package com.pago.pasarela.jpa;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
@NamedQuery(name="Compraws.valor", query="SELECT u FROM Compra u WHERE u.valor = :valor"),
@NamedQuery(name="Compraws.findAll", query="SELECT u FROM Compra u")
})
public class CompraWS implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@NotNull
	private float valor;

	@NotNull
	private Date fecha_hora;
	
	@NotNull
	private String id_compra_empresa;	
	

	private Tarjeta tarjeta;	
	
	public CompraWS() {}
	
	public CompraWS(float valor, Date fecha_hora, String id_compra_empresa,Tarjeta tarjeta) {
		super();
		this.valor = valor;
		this.fecha_hora = fecha_hora;
		this.id_compra_empresa = id_compra_empresa;
		this.tarjeta = tarjeta;

	}

	public Tarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public Date getFecha_hora() {
		return fecha_hora;
	}

	public void setFecha_hora(Date fecha_hora) {
		this.fecha_hora = fecha_hora;
	}

	public String getId_compra_empresa() {
		return id_compra_empresa;
	}

	public void setId_compra_empresa(String id_compra_empresa) {
		this.id_compra_empresa = id_compra_empresa;
	}


	
	

	
}
