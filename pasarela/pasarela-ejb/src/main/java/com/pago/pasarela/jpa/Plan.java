package com.pago.pasarela.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@NamedQueries({
@NamedQuery(name="Plan.findAll", query="SELECT p FROM Plan p")
})
public class Plan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@NotNull
	private int cuotas;
		
	public Plan() {}
	
	public Plan(int cuotas) {
		super();
		this.cuotas = cuotas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCuotas() {
		return cuotas;
	}

	public void setCuotas(int cuotas) {
		this.cuotas = cuotas;
	}
	
}
