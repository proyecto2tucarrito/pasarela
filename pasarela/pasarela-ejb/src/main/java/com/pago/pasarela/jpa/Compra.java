package com.pago.pasarela.jpa;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
@NamedQuery(name="Compra.valor", query="SELECT u FROM Compra u WHERE u.valor = :valor"),
@NamedQuery(name="Compra.findAll", query="SELECT u FROM Compra u")
})
public class Compra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@NotNull
	private float valor;

	@NotNull
	private Date fecha_hora;
	
	@NotNull
	private String id_compra_empresa;	
	

	private TarjetaCliente tarjeta;	
	
	public Compra() {}
	
	public Compra(float valor, Date fecha_hora, String id_compra_empresa,TarjetaCliente tarjeta) {
		super();
		this.valor = valor;
		this.fecha_hora = fecha_hora;
		this.id_compra_empresa = id_compra_empresa;
		this.tarjeta = tarjeta;

	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public TarjetaCliente getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(TarjetaCliente tarjeta) {
		this.tarjeta = tarjeta;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public Date getFecha_hora() {
		return fecha_hora;
	}

	public void setFecha_hora(Date fecha_hora) {
		this.fecha_hora = fecha_hora;
	}

	public String getId_compra_empresa() {
		return id_compra_empresa;
	}

	public void setId_compra_empresa(String id_compra_empresa) {
		this.id_compra_empresa = id_compra_empresa;
	}


	
	

	
}
