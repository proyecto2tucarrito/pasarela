
package com.pago.pasarela.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
@NamedQuery(name="Tarjeta.existe", query="SELECT t FROM Tarjeta t WHERE t.id = :id"),
@NamedQuery(name="Tarjeta.findAll", query="SELECT t FROM Tarjeta t")
})
public class Tarjeta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	
	@NotNull
	private String mesVenc;
	@NotNull
	private String anioVenc;	
	@NotNull
	private String codigo;	
	
	public Tarjeta() {}
	
	public Tarjeta(String id, String mesVenc,String anioVenc, String codigo) {
		super();
		this.id = id;
		this.mesVenc = mesVenc ;
		this.anioVenc = anioVenc ;
		this.codigo = codigo;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMesVenc() {
		return mesVenc;
	}

	public void setMesVenc(String mesVenc) {
		this.mesVenc = mesVenc;
	}

	public String getAnioVenc() {
		return anioVenc;
	}

	public void setAnioVenc(String anioVenc) {
		this.anioVenc = anioVenc;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
