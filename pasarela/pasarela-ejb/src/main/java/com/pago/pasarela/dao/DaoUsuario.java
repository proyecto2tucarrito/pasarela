package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.Usuario;

public class DaoUsuario implements IDaoUsuario {

	public DaoUsuario(){
		
	}
	
	@Override
	public void insert(EntityManager em, Usuario usuario) throws Exception {
		em.persist(usuario);
	}

	@Override
	public Usuario getUsuarioById(EntityManager em, String idUsr) throws Exception {
		return em.find(Usuario.class, idUsr);
	}

	@Override
	public void update(EntityManager em, Usuario usuario) throws Exception {
		em.merge(usuario);
	}

	@Override
	public void delete(EntityManager em, String idUsr) throws Exception {
		em.remove(getUsuarioById(em, idUsr));
	}
	
	
	@Override
	public List<Usuario> getAllUsuario(EntityManager em) throws Exception {
		return em.createNamedQuery("Usuario.findAll",Usuario.class).getResultList();
	}
	
	@Override
	public Usuario getUsuarioLogin(EntityManager em, String email, byte[] pass) throws Exception {
		return em.createNamedQuery("Usuario.login",Usuario.class)
					.setParameter("email", email)
					.setParameter("pass", pass).getSingleResult();
	}

	

}
