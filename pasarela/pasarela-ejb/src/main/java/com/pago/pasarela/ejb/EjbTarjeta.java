package com.pago.pasarela.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.pago.pasarela.dao.DaoTarjeta;
import com.pago.pasarela.dao.IDaoTarjeta;
import com.pago.pasarela.jpa.Tarjeta;

@Stateless
@LocalBean
public class EjbTarjeta implements IEjbTarjeta {

	@PersistenceContext
	private EntityManager em = null;
	
	public EjbTarjeta()
	{
	}
	
	@Override
	public void insert(Tarjeta tarjeta) throws Exception {
		IDaoTarjeta iDaoTarjeta = new DaoTarjeta();
		try {
			
			System.out.println("tarjeta "+tarjeta.getCodigo());
			System.out.println(tarjeta.getId());
			System.out.println(tarjeta.getMesVenc());
			System.out.println(tarjeta.getAnioVenc());
			iDaoTarjeta.insert(em, tarjeta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Tarjeta getTarjetaById(String idTarjeta) throws Exception {
		IDaoTarjeta iDaoTarjeta = new DaoTarjeta();
		try {
			return iDaoTarjeta.getTarjetaById(em, idTarjeta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	

	@Override
	public void update(Tarjeta tarjeta) throws Exception {
		IDaoTarjeta iDaoTarjeta = new DaoTarjeta();
		try {
			iDaoTarjeta.update(em, tarjeta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	@Override
	public boolean existeTarjeta(String idTarjeta) throws Exception {
		IDaoTarjeta iDaoTarjeta = new DaoTarjeta();
		try {
			Tarjeta tarjeta=iDaoTarjeta.getTarjetaById(em, idTarjeta);
			return tarjeta.equals(null);					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
}
