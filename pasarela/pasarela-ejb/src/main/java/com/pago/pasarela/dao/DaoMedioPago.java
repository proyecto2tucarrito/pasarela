package com.pago.pasarela.dao;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.MedioPago;
import com.pago.pasarela.jpa.Plan;

public class DaoMedioPago implements IDaoMedioPago {

	public DaoMedioPago(){
		
	}
	
	@Override
	public void insert(EntityManager em, MedioPago medioPago) throws Exception {
		System.out.println(medioPago.getId());
		List<Plan> lp=medioPago.getPlan();
		Iterator iter = lp.iterator();
		while(iter.hasNext()){
		  Plan planitem = (Plan)iter.next(); 
		  em.persist(planitem);
		}		
		
		em.persist(medioPago);
	}

	@Override
	public MedioPago getMedioPago(EntityManager em, String nombre) throws Exception {
		return em.find(MedioPago.class, nombre);
	}

	@Override
	public void update(EntityManager em, MedioPago medioPago) throws Exception {
		em.merge(medioPago);
	}

	@Override
	public void delete(EntityManager em, String nombre) throws Exception {
		em.remove(getMedioPago(em, nombre));
	}
	
	@Override
	public List<MedioPago> getAllMedioPago(EntityManager em) throws Exception {
		return em.createNamedQuery("MedioPago.findAll",MedioPago.class).getResultList();
	}

}
