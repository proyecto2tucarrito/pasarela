package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.Tarjeta;

public class DaoTarjeta implements IDaoTarjeta{

	public DaoTarjeta() {
	
	}
	@Override
	public void insert(EntityManager em, Tarjeta tarjeta) {
		em.persist(tarjeta);
	}
	
	@Override
	public Tarjeta getTarjetaById(EntityManager em, String idTarjeta) {
		return em.find(Tarjeta.class, idTarjeta);
	}
	
	@Override
	public void update(EntityManager em, Tarjeta tarjeta) {
		em.merge(tarjeta);
	}
	
}