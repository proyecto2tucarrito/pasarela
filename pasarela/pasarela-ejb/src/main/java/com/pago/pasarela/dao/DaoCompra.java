package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.Compra;

public class DaoCompra implements IDaoCompra{

	public DaoCompra() {
	
	}
	@Override
	public void insert(EntityManager em, Compra compra) {
		em.persist(compra);
	}
	
	@Override
	public Compra getCompraById(EntityManager em, int idCompra) {
		return em.find(Compra.class, idCompra);
	}
	
	@Override
	public void update(EntityManager em, Compra compra) {
		em.merge(compra);
	}
	
	@Override
	public void delete(EntityManager em, int idCompra) {
		em.remove(getCompraById(em, idCompra));
	}
	
	@Override
	public List<Compra> getAllCompra(EntityManager em){
		return em.createNamedQuery("Compra.findAll",Compra.class).getResultList();
	}
	
}
