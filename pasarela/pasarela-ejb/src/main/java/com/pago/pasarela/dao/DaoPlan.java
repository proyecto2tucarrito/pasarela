package com.pago.pasarela.dao;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.Plan;

public class DaoPlan implements IDaoPlan{

	public DaoPlan(){
		
	}
	
	@Override
	public void insert(EntityManager em, Plan plan) throws Exception {
		em.persist(plan);
	}

	@Override
	public Plan getPlan(EntityManager em, int id) throws Exception {
		return em.find(Plan.class, id);
	}

	@Override
	public void update(EntityManager em, Plan plan) throws Exception {
		em.merge(plan);
	}

	@Override
	public void delete(EntityManager em, int id) throws Exception {
		em.remove(getPlan(em, id));
	}
	

	

}
