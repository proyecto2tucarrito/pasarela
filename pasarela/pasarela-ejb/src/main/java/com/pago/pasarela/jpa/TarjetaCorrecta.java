
package com.pago.pasarela.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
@NamedQuery(name="TarjetaCorrecta.existe", query="SELECT t FROM TarjetaCorrecta t WHERE t.numero = :numero"),
@NamedQuery(name="TarjetaCorrecta.findAll", query="SELECT t FROM TarjetaCorrecta t")
})
public class TarjetaCorrecta implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	private byte[] numero;
	
	@NotNull
	private byte[] mesVenc;

	@NotNull
	private byte[] anioVenc;	
	
	@NotNull
	private byte[] codigo;	
	
	public TarjetaCorrecta() {}
	
	public TarjetaCorrecta(byte[] numero, byte[] mesVenc, byte[] anioVenc, byte[] codigo) {
		super();
		this.numero = numero;
		this.mesVenc = mesVenc;
		this.anioVenc = anioVenc;
		this.codigo = codigo;

	}

	public byte[] getNumero() {
		return numero;
	}

	public void setNumero(byte[] numero) {
		this.numero = numero;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getMesVenc() {
		return mesVenc;
	}

	public void setMesVenc(byte[] mesVenc) {
		this.mesVenc = mesVenc;
	}

	public byte[] getAnioVenc() {
		return anioVenc;
	}

	public void setAnioVenc(byte[] anioVenc) {
		this.anioVenc = anioVenc;
	}

	public byte[] getCodigo() {
		return codigo;
	}

	public void setCodigo(byte[] codigo) {
		this.codigo = codigo;
	}

}
