package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.MedioPago;

public interface IDaoMedioPago {
	public void insert(EntityManager em, MedioPago medioPago) throws Exception;
	public MedioPago getMedioPago(EntityManager em, String nombre) throws Exception;
	public void update(EntityManager em, MedioPago medioPago) throws Exception;
	public void delete(EntityManager em, String nombre) throws Exception;
	public List<MedioPago> getAllMedioPago(EntityManager em) throws Exception;
	
}
