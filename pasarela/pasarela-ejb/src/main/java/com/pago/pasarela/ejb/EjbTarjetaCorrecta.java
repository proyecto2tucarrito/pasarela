package com.pago.pasarela.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.pago.pasarela.dao.DaoTarjetaCorrecta;
import com.pago.pasarela.dao.IDaoTarjetaCorrecta;
import com.pago.pasarela.jpa.Compra;
import com.pago.pasarela.jpa.CompraWS;
import com.pago.pasarela.jpa.Tarjeta;
import com.pago.pasarela.jpa.TarjetaCliente;
import com.pago.pasarela.jpa.TarjetaCorrecta;



@Stateless
@LocalBean
public class EjbTarjetaCorrecta implements IEjbTarjetaCorrecta {

	@PersistenceContext
	private EntityManager em = null;
	
	public EjbTarjetaCorrecta()
	{
	}
	
	@Override
	public void insert(Tarjeta tarjetaWS) throws Exception {
		IDaoTarjetaCorrecta iDaoTarjeta = new DaoTarjetaCorrecta();		
		try {
			TarjetaCorrecta tarjeta=encriptarTarjeta(tarjetaWS);
			iDaoTarjeta.insert(em, tarjeta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
	}
	
	@Override
	public TarjetaCorrecta getTarjetaById(byte[] numeroTarjeta) throws Exception {
		IDaoTarjetaCorrecta iDaoTarjeta = new DaoTarjetaCorrecta();
		try {
			return iDaoTarjeta.getTarjetaById(em, numeroTarjeta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	

	@Override
	public void update(Tarjeta tarjetaWS) throws Exception {
		IDaoTarjetaCorrecta iDaoTarjeta = new DaoTarjetaCorrecta();
		try {
			TarjetaCorrecta tarjeta=encriptarTarjeta(tarjetaWS);
			iDaoTarjeta.update(em, tarjeta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	@Override
	public boolean existeTarjeta(byte[] numeroTarjeta) throws Exception {
		IDaoTarjetaCorrecta iDaoTarjeta = new DaoTarjetaCorrecta();
		try {
			TarjetaCorrecta tarjeta=iDaoTarjeta.getTarjetaById(em, numeroTarjeta);
			return tarjeta.equals(null);					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public TarjetaCorrecta encriptarTarjeta(Tarjeta tarjetaWS) throws Exception {
		TarjetaCorrecta tarjeta= new TarjetaCorrecta();
		String clave2="770A8A65DA156D24EE2A093277530142";
	    String clave1="2233445566778899001122";
	    Encriptacion enc =new Encriptacion();
		System.out.println(tarjetaWS.getCodigo());
		//System.out.println(tarjetaWS.getFecha_venc().toString());
		System.out.println(tarjetaWS.getId());
	    byte[] codigo= enc.encryptAes(clave1,clave2,tarjetaWS.getCodigo());
		byte[] anioVenc= enc.encryptAes(clave1,clave2,tarjetaWS.getAnioVenc().toString());
		byte[] mesVenc= enc.encryptAes(clave1,clave2,tarjetaWS.getMesVenc().toString());
		byte[] numero= enc.encryptAes(clave1,clave2,tarjetaWS.getId());
		System.out.println(codigo);
		System.out.println(anioVenc);
		System.out.println(mesVenc);
		System.out.println(numero);
		
		tarjeta.setCodigo(codigo);
		tarjeta.setAnioVenc(anioVenc);
		tarjeta.setMesVenc(mesVenc);
		tarjeta.setNumero(numero);
		return tarjeta;
		
	}	
	
}
