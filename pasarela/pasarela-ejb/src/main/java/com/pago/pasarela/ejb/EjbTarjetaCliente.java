package com.pago.pasarela.ejb;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.pago.pasarela.dao.DaoTarjetaCliente;
import com.pago.pasarela.dao.IDaoTarjetaCliente;
import com.pago.pasarela.jpa.TarjetaCliente;



@Stateless
@LocalBean
public class EjbTarjetaCliente implements IEjbTarjetaCliente {

	@PersistenceContext
	private EntityManager em = null;
	
	public EjbTarjetaCliente()
	{
	}
	
	@Override
	public void insert(TarjetaCliente tarjeta) throws Exception {
		IDaoTarjetaCliente iDaoTarjeta = new DaoTarjetaCliente();
		try {
			iDaoTarjeta.insert(em, tarjeta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
	}
	
	@Override
	public TarjetaCliente getTarjetaById(byte[] numeroTarjeta) throws Exception {
		IDaoTarjetaCliente iDaoTarjeta = new DaoTarjetaCliente();
		try {
			return iDaoTarjeta.getTarjetaById(em, numeroTarjeta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	

	@Override
	public void update(TarjetaCliente tarjeta) throws Exception {
		IDaoTarjetaCliente iDaoTarjeta = new DaoTarjetaCliente();
		try {
			iDaoTarjeta.update(em, tarjeta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	@Override
	public boolean existeTarjeta(byte[] numeroTarjeta) throws Exception {
		IDaoTarjetaCliente iDaoTarjeta = new DaoTarjetaCliente();
		try {
			TarjetaCliente tarjeta=iDaoTarjeta.getTarjetaById(em, numeroTarjeta);
			return tarjeta.equals(null);					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
}
