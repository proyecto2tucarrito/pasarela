package com.pago.pasarela.ejb;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.pago.pasarela.dao.DaoMedioPago;
import com.pago.pasarela.dao.IDaoMedioPago;
import com.pago.pasarela.jpa.MedioPago;

@Stateless
@LocalBean
public class EjbMedioPago implements IEjbMedioPago {
	
	@PersistenceContext
	private EntityManager em = null;
	
	public EjbMedioPago()
	{
	}
	
	@Override
	public void insert(MedioPago medioPago) throws Exception {
		IDaoMedioPago iDaoMedioPago = new DaoMedioPago();
		try {
			iDaoMedioPago.insert(em, medioPago);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
	}
	
	@Override
	public MedioPago getMedioPago(String id) throws Exception {
		IDaoMedioPago iDaoMedioPago = new DaoMedioPago();
		try {
			return iDaoMedioPago.getMedioPago(em, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void update(MedioPago medioPago) throws Exception {
		IDaoMedioPago iDaoMedioPago = new DaoMedioPago();
		try {
			iDaoMedioPago.update(em, medioPago);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void delete(String id) throws Exception {
		IDaoMedioPago iDaoMedioPago = new DaoMedioPago();
		try {
			iDaoMedioPago.delete(em, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	public List<MedioPago> getAllMedioPago() throws Exception {
		IDaoMedioPago iDaoMedioPago = new DaoMedioPago();
		try {
			return iDaoMedioPago.getAllMedioPago(em);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	

}

