package com.pago.pasarela.ejb;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.pago.pasarela.dao.DaoCompra;
import com.pago.pasarela.dao.DaoTarjetaCliente;
import com.pago.pasarela.dao.DaoTarjetaCorrecta;
import com.pago.pasarela.ejb.Encriptacion;

import com.pago.pasarela.dao.IDaoTarjetaCliente;
import com.pago.pasarela.dao.IDaoTarjetaCorrecta;
import com.pago.pasarela.dao.IDaoCompra;
import com.pago.pasarela.jpa.Compra;
import com.pago.pasarela.jpa.CompraWS;
import com.pago.pasarela.jpa.TarjetaCliente;
import com.pago.pasarela.jpa.TarjetaCorrecta;



@Stateless
@LocalBean
public class EjbCompra implements IEjbCompra {

	@PersistenceContext
	private EntityManager em = null;
	
	public EjbCompra()
	{
	}
	
	@Override
	public void insert(CompraWS compraWS) throws Exception {
		IDaoCompra iDaoCompra = new DaoCompra();
		
		try {
			IDaoTarjetaCliente iDaoTarjetaCliente = new DaoTarjetaCliente();	
			IDaoTarjetaCorrecta iDaoTarjetaCorrecta = new DaoTarjetaCorrecta();
			
			Compra compra =encriptarCompra(compraWS);
			TarjetaCliente tarjeta=iDaoTarjetaCliente.getTarjetaById(em, compra.getTarjeta().getNumero());
			TarjetaCorrecta tarjetaCorrecta=iDaoTarjetaCorrecta.getTarjetaById(em, compra.getTarjeta().getNumero());			
			if (tarjetaCorrecta!=null)
			{
				if (tarjeta==null)
				{
					System.out.println(compra.getId_compra_empresa());
					System.out.println(compra.getValor());
					System.out.println(compra.getFecha_hora());
					System.out.println(compra.getTarjeta().getId());
					System.out.println(compra.getTarjeta().getCodigo());
					System.out.println(compra.getTarjeta().getNumero());
					System.out.println(compra.getTarjeta().getAnioVenc());
					System.out.println(compra.getTarjeta().getMesVenc());
					if (compra.getTarjeta()!=null){
						System.out.println("Inserta tarjeta");
						iDaoTarjetaCliente.insert(em, compra.getTarjeta());
					}
				}
				else
				{
					System.out.println("Ya existe la tarjeta.Se innseta solo la compra");
				}
				System.out.println("Inserta compra");
			iDaoCompra.insert(em, compra);}
			else
			{
				System.out.println("Tarjeta incorrecta. Compra invalida");				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Compra getCompraById(int idCompra) throws Exception {
		IDaoCompra iDaoCompra = new DaoCompra();
		try {
			return iDaoCompra.getCompraById(em, idCompra);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	

	@Override
	public void update(Compra compra) throws Exception {
		IDaoCompra iDaoCompra = new DaoCompra();
		try {
			iDaoCompra.update(em, compra);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	@Override
	public void delete(int idCompra) throws Exception {
		IDaoCompra iDaoCompra = new DaoCompra();
		try {
			iDaoCompra.delete(em, idCompra);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Compra> getAllCompra() throws Exception {
		IDaoCompra iDaoCompra = new DaoCompra();
		try {
			return iDaoCompra.getAllCompra(em);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	
	
	@Override
	public Compra encriptarCompra(CompraWS compraWs) throws Exception {
		Compra compra= new Compra();
		compra.setId_compra_empresa(compraWs.getId_compra_empresa());
		compra.setFecha_hora(compraWs.getFecha_hora());
		compra.setValor(compraWs.getValor());
		TarjetaCliente tarjeta= new TarjetaCliente();
		String clave2="770A8A65DA156D24EE2A093277530142";
	    String clave1="2233445566778899001122";
	    Encriptacion enc =new Encriptacion();
		System.out.println(compraWs.getTarjeta().getCodigo());
		//System.out.println(compraWs.getTarjeta().getFecha_venc().toString());
		System.out.println(compraWs.getTarjeta().getId());
	    byte[] codigo= enc.encryptAes(clave1,clave2,compraWs.getTarjeta().getCodigo());
		byte[] anioVenc= enc.encryptAes(clave1,clave2,compraWs.getTarjeta().getAnioVenc().toString());
		byte[] mesVenc= enc.encryptAes(clave1,clave2,compraWs.getTarjeta().getMesVenc().toString());
		byte[] numero= enc.encryptAes(clave1,clave2,compraWs.getTarjeta().getId());
		System.out.println(codigo);
		System.out.println(numero);
		
		tarjeta.setCodigo(codigo);
		tarjeta.setAnioVenc(anioVenc);
		tarjeta.setMesVenc(mesVenc);
		tarjeta.setNumero(numero);
		compra.setTarjeta(tarjeta);
		return compra;
		
	}
	
}
