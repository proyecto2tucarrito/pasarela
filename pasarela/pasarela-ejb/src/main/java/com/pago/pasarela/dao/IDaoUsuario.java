package com.pago.pasarela.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.pago.pasarela.jpa.Usuario;

public interface IDaoUsuario {
	public void insert(EntityManager em, Usuario usuario) throws Exception;
	public Usuario getUsuarioById(EntityManager em, String idUsr) throws Exception;
	public void update(EntityManager em, Usuario usuario) throws Exception;
	public void delete(EntityManager em, String idUsr) throws Exception;
	public List<Usuario> getAllUsuario(EntityManager em) throws Exception;
	public Usuario getUsuarioLogin(EntityManager em, String nombre, byte[] pass) throws Exception;
	
}
