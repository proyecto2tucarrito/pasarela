package com.pago.pasarela.ejb;

import java.util.List;

import javax.ejb.Local;

import com.pago.pasarela.jpa.TarjetaCliente;

@Local
public interface IEjbTarjetaCliente {
	
	public void insert(TarjetaCliente tarjeta) throws Exception;
	public TarjetaCliente getTarjetaById(byte[] numeroTarjeta) throws Exception;
	public void update(TarjetaCliente tarjeta) throws Exception;
	public boolean existeTarjeta(byte[] numeroTarjeta) throws Exception;
	
}