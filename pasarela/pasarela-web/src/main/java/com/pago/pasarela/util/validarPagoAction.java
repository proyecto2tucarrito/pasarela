package com.pago.pasarela.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.pago.pasarela.ejb.IEjbCompra;
import com.pago.pasarela.ejb.IEjbTarjeta;
import com.pago.pasarela.jpa.Tarjeta;

@RequestScoped
@Named("validarPago")
public class validarPagoAction {

    private Logger log = Logger.getLogger(validarPagoAction.class.getName());

    private compraF compra;
    
    private Tarjeta tarjeta;

    @EJB
    private IEjbTarjeta ejbTarjeta;
    
	@EJB
	private IEjbCompra ejbCompra;    
    
    @PostConstruct
    public void init() {
    	compra = new compraF();
    	tarjeta= new Tarjeta();
    	tarjeta.setAnioVenc("2018");
    	tarjeta.setMesVenc("02");
    	tarjeta.setCodigo("345");
    	System.out.println("FDFSFSD");
    	tarjeta.setId("7896");
        log.info("Inicializando cuenta con valores por defecto");
    }

    @Produces
    @Named
    @RequestScoped
    public compraF getCompraF() {
        return compra;
    }
    @Produces
    @Named
    @RequestScoped
    public Tarjeta getTarjeta() {
        return tarjeta;
    }
    
    public String crearCompra() {
        log.info("Guardando");
        log.info(tarjeta.getId());
        log.info(tarjeta.getCodigo());
        log.info(tarjeta.getMesVenc());
        log.info(tarjeta.getAnioVenc());
		try {
			log.info(tarjeta.getCodigo());
			ejbTarjeta.insert(tarjeta);
			return "index.xhtml";
		} catch (Exception e) {
			log.info(" sfsdf "+e.getMessage());
			return "index.xhtml";	
		}
        
    }

}

