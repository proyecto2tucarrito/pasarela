package com.pago.pasarela.rest;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.pago.pasarela.ejb.IEjbTarjeta;
import com.pago.pasarela.jpa.Tarjeta;

@RequestScoped
@Path("tarjeta")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class TarjetaRS {

	@EJB
	private IEjbTarjeta ejbTarjeta;
	
	@POST
	@Path("registro")
	public Response create(final Tarjeta tarjeta) {

		try {
			ejbTarjeta.insert(tarjeta);
			return Response
					.ok(tarjeta)
					.build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}

