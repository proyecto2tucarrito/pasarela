package com.pago.pasarela.rest;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.pago.pasarela.ejb.IEjbMedioPago;
import com.pago.pasarela.jpa.MedioPago;

@RequestScoped
@Path("mediopago")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class MedioPagoRS {

	@EJB
	private IEjbMedioPago ejbMedioPago;
	
	@POST
	@Path("registro")
	public Response create(final MedioPago medioPago) {

		try {
			ejbMedioPago.insert(medioPago);
			return Response
					.ok(medioPago)
					.build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
