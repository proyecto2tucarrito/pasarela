package com.pago.pasarela.rest;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.pago.pasarela.ejb.IEjbPlan;
import com.pago.pasarela.jpa.Plan;

@RequestScoped
@Path("plan")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class PlanRS {

	@EJB
	private IEjbPlan ejbPlan;
	
	@POST
	@Path("registro")
	public Response create(final Plan plan) {

		try {
			ejbPlan.insert(plan);
			return Response
					.ok(plan)
					.build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}


