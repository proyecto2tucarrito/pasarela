package com.pago.pasarela.util;
import com.pago.pasarela.jpa.Tarjeta;
import java.util.Date;

		public class compraF { 
			
			private int id;

			private float valor;

			private Date fecha_hora;
			
			private String id_compra_empresa;	

			private Tarjeta tarjeta;	
			
			public compraF() {}
			
			public compraF(float valor, Date fecha_hora, String id_compra_empresa,Tarjeta tarjeta) {
				super();
				this.valor = valor;
				this.fecha_hora = fecha_hora;
				this.id_compra_empresa = id_compra_empresa;
				this.tarjeta = tarjeta;

			}

			public Tarjeta getTarjeta() {
				return tarjeta;
			}

			public void setTarjeta(Tarjeta tarjeta) {
				this.tarjeta = tarjeta;
			}

			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			public float getValor() {
				return valor;
			}

			public void setValor(float valor) {
				this.valor = valor;
			}

			public Date getFecha_hora() {
				return fecha_hora;
			}

			public void setFecha_hora(Date fecha_hora) {
				this.fecha_hora = fecha_hora;
			}

			public String getId_compra_empresa() {
				return id_compra_empresa;
			}

			public void setId_compra_empresa(String id_compra_empresa) {
				this.id_compra_empresa = id_compra_empresa;
			}


			
			

			
		}
