package com.pago.pasarela.rest;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.net.URI;
import java.net.URL;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.spi.Context;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import com.pago.pasarela.ejb.IEjbCompra;
import com.pago.pasarela.jpa.CompraWS;

@RequestScoped
@Path("compra")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class CompraRS {

	@EJB
	private IEjbCompra ejbCompra;
	
	@POST
	@Path("registro")
	public Response create(final CompraWS compra) {

		try {
			System.out.println("compra");
			ejbCompra.insert(compra);
			FacesContext context  = FacesContext.getCurrentInstance();
			context.getExternalContext().redirect("http://stackoverflow.com");
//		    externalContext.redirect("http://stackoverflow.com");
			System.out.println("compra2222222");
			
			
			
			return Response.status(307).build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@POST
	@Path("compraDatos")
	public Response compra(final String compra) {

		try {
			System.out.println("compra");
			String path = "C:\\file.txt";
			BufferedWriter out = new BufferedWriter(new FileWriter(path));
			out.write(compra);
			out.close();
			return Response.ok("QWERTYUIOPLKJHGFDDSAMNBVCXz").build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}	
	
	
}


