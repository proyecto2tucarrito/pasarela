package com.pago.pasarela.soap;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
@WebService(name="prueba")
public class Hola {
	
	@WebMethod(operationName = "hello")
	public String hello(@WebParam(name = "name") String txt) 
	{
		return "Hola " + txt + " !";
	}

}
